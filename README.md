# proof of learning implementation 
# Proof of Learning 

The consensus algorithm is the core component of a blockchain system, which determines the efficiency, security, and scalability of the blockchain network. The representative consensus algorithm is the proof of work (PoW) proposed in Bitcoin, where the consensus process consumes large amount of compute in solving meaningless Hash puzzel. Meanwhile, the deep learning (DL) has brought unprecedented performance gains at heavy computate cost. In this demo, we channels the otherwise wasted computational power to the practical purpose of training neural network models, through the proposed proof of learning (PoL) consensus algorithm. In PoLe, the training/testing data are released to the entire blockchain network (BCN) and the consensus nodes train NN models on the data, which serves as the proof of learning. When the consensus on the BCN considers a NN model to be valid, a new block is appended to the blockchain. Through our system, we investigate the potential of enpowering machine learning with consensus building on blockchains.

## Introduction 
A blockchain network (BCN) is a decentralized distributed
system where participants are not necessary trustworthy but able to collectively maintain a consistent database
or ledger . The core component of a BCN is the consensus algorithm. The pioneering
BCN is Bitcoin and its consensus algorithm is proof of work
(PoW) (Nakamoto 2008). The PoW has been applied in various applications scenerios, such as financial services, and
alternative cryptocurrency. However, a major drawback of
a PoW based BCN is that the system consumes massive
amount of compute and energy (around 15M $ / day).Most of the energy is spent on calculating the nonce of hash functions, which serves no
real purposes other than being difficult to compute.
On the other hand, machine learning, especially deeplearning, has been widely applied in many fields, such as business and manufacturing (Khan et al. 2020; Lin et al. 2020; Gai et al. 2020). Deep learning uses labeled data to train deep learning models in the way of iteratively updating network weights. A well structured deep learning model
can fit any function, which makes deep-learning present
strong productivity in solving specific problems.However,the training of deep learning model usually consumes a lot of computing resources. With the extensive application of deep learning, the demand of computing power will be more and more. Meanwhile, significant performance gain in deep learning has been derived from scaling up the network and training data (Mahajan et al. 2018; Radford et al. 2019; De-vlin et al. 2019; Yalniz et al. 2019) and automatic design of neural network (NN) architectures (So et al. 2019; Real et al. 2019). These trends have created an ever-growing demand for computational power. In this demo, we present a system which directs the computation and energy spent on blockchain consensus to the practical function of training machine learning models. We build the system based on our proposed proof of learning (PoLe) consensus algorithm where an asymmetric puzzle is designed to generate tampering prevention blocks (Lan, Liu, and Li 2020). The proposed PoLe-based BCN provides a platform on which users may commission a neural network model that meets their requirements. The BCN contains two types of participants, data nodes which announce tasks with a reward, and consensus nodes or miners which work to solve the announced tasks. After a data node announces a task, consensus nodes may accept it and seek a model that meets the announced minimum training accuracy. After receiving a valid solution which meets the training accuracy criterion, the data node releases the test set. The consensus nodes then collectively select the solution with the highest generalization performance and distribute the task reward accordingly. Thus, a PoLe-based blockchain can serve as a decentralized database and a machine-learning platform si-multaneously.

## network design

At first glance, peer-to-peer decentralized network applications are complex and difficult. While you need to provide some networking functionality on application level, the architecture is really simple. You have a network of the "same" nodes. The "same" means the same application (or an application that implements the same protocol).

Nodes are connected with each other. This means that each node provides a TCP/IP server on a specific port to provide inbound nodes to connect. The same node is able to connect to other nodes; called outbound nodes. When a node has a lot of connections with nodes in the network, the node will get most likely the required messages. You are able to send a message over the TCP/IP channel to the connected (inbound and outbound) nodes. How they react to the messages is in your hands. When you would like to implement discovery, meaning to see which nodes are connected within the network and see if you would like to connect to those, you need to relay this message to the other nodes connected to you. Note that you need to make sure that the messages will not echo around, but that you keep track which messages you have received.

How to optimize these node connections depend on what you would like to solve. When providing file sharing, you would like to have a lot of connections when nodes have a large bandwith. However, when you are running Bitcoin, you would like to have your connections spread over the world to minimize the single identity problem.

## Events that can occur

### outbound_node_connected
The node connects with another node - ````node.connect_with_node('127.0.0.1', 8002)```` - and the connection is successful. While the basic functionality is to exchange the node id's, no user data is involved.

### inbound_node_connected
Another node has made a connection with this node and the connection is successful. While the basic functionality is to exchange the node id's, no user data is involved.

### outbound_node_disconnected
A node, to which we had made a connection in the past, is disconnected.

### inbound_node_disconnected
A node, that had made a connection with us in the past, is disconnected.

### node_message
A node - ```` connected_node ```` - sends a message. At this moment the basic functionality expects JSON format. It tries to decode JSON when the message is received. If it is not possible, the message is rejected.

### node_disconnect_with_outbound_node
The application actively wants to disconnect the outbound node, a node with which we had made a connection in the past. You could send some last message to the node, that you are planning to disconnect, for example.

### node_request_to_stop
The main node, also the application, is stopping itself. Note that the variable connected_node is empty, while there is no connected node involved.

# Debugging

When things go wrong, you could enable debug messages of the Node class. The class shows these messages in the console and shows all the details of what happens within the class. To enable debugging for a node, use the code example below.

````python
node = Node("127.0.0.1", 10001)
node.debug = True
````

## Installation

this project requires the following dependencies to run :
```bash
pip install numpy
pip install pytorch==1.6.0
pip install torchvision==0.7.0
pip install scipy==1.6.0
```

## Usage
### To train a model and create a proof-of-learning by youtself:

```python
python backend/train.py --save-freq [checkpointing interval] --dataset [any dataset in torchvision] --model [models defined in model.py or any torchvision model]
```
`save-freq` is checkpointing interval, denoted by k in the paper. There are a few other arguments that you could find at the end of the script.

Note that the proposed algorithm does not interact with the training process, so it could be applied to any kinds of gradient-descent based models.
### To verify a given proof-of-learning by youtself:

```python
python backend/verify.py --model-dir [path/to/the/proof] --dist [distance metric] --q [query budget] --delta [slack parameter]

```
Setting q to 0 or smaller will verify the whole proof, otherwise the top-q iterations for each epoch will be verified. More information about `q` and `delta` can be found in the paper. For `dist`, you could use one or more of `1`, `2`, `inf`, `cos` (if more than one, separate them by space). The first 3 are corresponding l_p norms, while `cos` is cosine distance. Note that if using more than one, the top-q iterations in terms of all distance metrics will be verified.

Please make sure `lr`, `batch-sizr`, `epochs`, `dataset`, `model`, and `save-freq` are consistent with what used in `train.py`.

### to run the network with all the nodes 

```python
python chain.py --model
```
 note : arg parsing will be add shortly
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
