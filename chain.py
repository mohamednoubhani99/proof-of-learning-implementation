import sys
import time
sys.path.insert(0, '..') # Import the files where the modules are located MyOwnPeer2PeerNode

from participants.active_node import ActiveNode

master_node = ActiveNode("127.0.0.1", 8001, 1,nodeType='master')
training_node = ActiveNode("127.0.0.1", 8002, 2,nodeType='training')
testing_node = ActiveNode("127.0.0.1", 8003, 3,nodeType='testing')

time.sleep(1)

master_node.start()
training_node.start()
testing_node.start()

time.sleep(1)

master_node.connect_with_node('127.0.0.1', 8002)
training_node.connect_with_node('127.0.0.1', 8003)
testing_node.connect_with_node('127.0.0.1', 8001)

time.sleep(2)
master_node.runTask()
training_node.runTask()
testing_node.runTask()
#master_node.send_to_nodes("message: Hi there!")

time.sleep(2)

print("node 1 is stopping..")
master_node.stop()

time.sleep(20)



time.sleep(10)

time.sleep(5)

master_node.stop()
training_node.stop()
testing_node.stop()
print('The test has ended')