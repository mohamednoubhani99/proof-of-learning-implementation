from network.node import Node
import backend.train as train
import backend.model as model
import backend.verify as verify

class ActiveNode(Node):

    # Python class constructor
    def __init__(self, host, port, id=None, callback=None, max_connections=0,nodeType="Unknown"):
        super(ActiveNode, self).__init__(host, port, id, callback, max_connections)
        self.nodeType = nodeType
        print(self.nodeType +  "_" +self.id + " : Started")

    # all the methods below are called when things happen in the network.
    # implement your network node behavior to create the required functionality.

    def outbound_node_connected(self, node):
        print("outbound_node_connected (" + self.id + "): " + node.id)
        
    def inbound_node_connected(self, node):
        print("inbound_node_connected: (" + self.id + "): " + node.id)

    def inbound_node_disconnected(self, node):
        print("inbound_node_disconnected: (" + self.id + "): " + node.id)

    def outbound_node_disconnected(self, node):
        print("outbound_node_disconnected: (" + self.id + "): " + node.id)

    def node_message(self, node, data):
        print("node_message (" + self.id + ") from " + node.id + ": " + str(data))
        
    def node_disconnect_with_outbound_node(self, node):
        print("node wants to disconnect with oher outbound node: (" + self.id + "): " + node.id)
        
    def node_request_to_stop(self):
        print("node is requested to stop (" + self.id + "): ")

    def runTask(self):
        if self.nodeType == 'master':
            self.send_to_nodes("message: start working on the Resnet10 model with this CIFAR-10 dataset")
        if self.nodeType == 'training':
            self.send_to_nodes("message: started training the model...")
            train.startTraining()
        if self.nodeType == 'testing':
            self.send_to_nodes("message: started testing the model")
            verify.startTesting()


    def sendData(datasetName):
        self.send_to_nodes({'data' : datasetName})
    
    def getDataset():
        pass