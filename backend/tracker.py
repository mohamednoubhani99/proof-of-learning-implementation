import requests

url = "https://fixer-fixer-currency-v1.p.rapidapi.com/latest"

querystring = {"base":"USD","symbols":"GBP,JPY,EUR"}

headers = {
    'x-rapidapi-host': "fixer-fixer-currency-v1.p.rapidapi.com",
    'x-rapidapi-key': "SIGN-UP-FOR-KEY"
    }

response = requests.request("GET", url, headers=headers, params=querystring)

print(response.text)